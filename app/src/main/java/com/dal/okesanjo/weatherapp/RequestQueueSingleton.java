package com.dal.okesanjo.weatherapp;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestQueueSingleton {

    private static RequestQueueSingleton mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    /**
     * Singleton class for Volley request queue.
     * @param context Context
     */
    private RequestQueueSingleton(Context context){
        mCtx = context.getApplicationContext();
        mRequestQueue = getRequestQueue();
    }

    /**
     * Return the current request queue.
     * @return RequestQueue
     */
    public RequestQueue getRequestQueue(){
        if (mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * Return this instance.
     * @param context Context
     * @return RequestQueueSingleton
     */
    public static synchronized RequestQueueSingleton getInstance(Context context){
        if (mInstance == null){
            mInstance = new RequestQueueSingleton(context.getApplicationContext());
        }
        return mInstance;
    }

    /**
     * Add a given Request to the Volley RequestQueue.
     * @param req Request
     * @param <T>
     */
    public <T> void addToRequestQueue(Request<T> req){
        getRequestQueue().add(req);
    }
}
