package com.dal.okesanjo.weatherapp;

import com.google.gson.annotations.SerializedName;

/**
 * Model representing cities that are listed in
 * the cities.json file from OpenWeatherMap.
 */
public class OpenWeatherCity {

    @SerializedName("id")
    long id;

    @SerializedName("name")
    String name;

    @SerializedName("country")
    String country;

    @SerializedName("coord")
    Coordinates coord;

    @Override
    public String toString(){
        return name + ", " + country;
    }
}

class Coordinates{
    @SerializedName("lon")
    double lon;

    @SerializedName("lat")
    double lat;
}
