package com.dal.okesanjo.weatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    // declare variables
    private SearchableSpinner spinner;
    private ImageView weatherIcon;
    private TextView cityNameTxt, curTempTxt, minTempTxt, maxTempTxt, humidityTxt, cloudTxt, weatherTxt, weatherDescTxt;

    private Runnable runnable;
    private ArrayAdapter<String> itemsAdapter;
    private OpenWeatherCity selectedCity;
    private OpenWeatherCity[] citiesList;
    private String[] citiesName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityNameTxt = findViewById(R.id.lblCityName);
        weatherTxt = findViewById(R.id.lblMainWeather);
        weatherDescTxt = findViewById(R.id.lblWeatherDesc);
        curTempTxt = findViewById(R.id.valCurTemp);
        minTempTxt = findViewById(R.id.valMinTemp);
        maxTempTxt = findViewById(R.id.valMaxTemp);
        weatherIcon = findViewById(R.id.weatherIcon);
        humidityTxt = findViewById(R.id.valHumidity);
        cloudTxt = findViewById(R.id.valClouds);

        String citiesJson = inputStreamToString(getResources().openRawResource(R.raw.cities));
        citiesList = (new Gson()).fromJson(citiesJson, OpenWeatherCity[].class);

        Arrays.sort(citiesList, new Comparator<OpenWeatherCity>() {
            @Override
            public int compare(OpenWeatherCity o1, OpenWeatherCity o2) {
                return o1.toString().compareToIgnoreCase(o2.toString());
            }
        });

        citiesName = new String[citiesList.length];
        for(int i= 0; i < citiesList.length; i++){
            citiesName[i] = citiesList[i].toString();
        }

        itemsAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, citiesName);

        spinner = findViewById(R.id.selCitySpinner);
        spinner.setTitle(getResources().getString(R.string.city_prompt));
        spinner.setPositiveButton(getResources().getString(R.string.btn_search));
        spinner.setAdapter(itemsAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = citiesList[position];
                Log.d("WeatherApp", selectedCity.name);
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        getCityWeather();
                    }
                };

                Thread thread = new Thread(null, runnable, "background");
                thread.start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

    }

    /**
     * Read the String represented by the given InputStream.
     * @param inputStream InputStream
     * @return String
     */
    public String inputStreamToString(InputStream inputStream){
        try{
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch(IOException e){
            return null;
        }
    }

    /**
     * Convert a given temperature from Kelvin to Celsius.
     * @param kelvin Double
     * @return String repr. of temperature.
     */
    public String getCelsius(double kelvin){
        return Double.toString(Math.round(kelvin - 273.15));
    }

    /**
     * Send and parse a given API request.
     */
    public void getCityWeather(){

        final String apiBase = "http://api.openweathermap.org/data/2.5/weather?id=";
        final String apiKey = "APPID=20c2e6d0a6b85f091c13744021084299";
        String apiUrl = apiBase + selectedCity.id + "&" + apiKey;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {

            private JSONObject weatherDesc, weatherTemp, weatherCloud, weatherSys;

            @Override
            public void onResponse(JSONObject response) {

                try{
                    weatherDesc = response.getJSONArray("weather").getJSONObject(0);
                    weatherTemp = response.getJSONObject("main");
                    weatherCloud = response.getJSONObject("clouds");
                    weatherSys = response.getJSONObject("sys");

                    String cityname = response.getString("name") + ", " + weatherSys.getString("country");

                    cityNameTxt.setText(cityname);
                    weatherTxt.setText(weatherDesc.getString("main"));
                    String icon = "im" + weatherDesc.getString("icon");
                    weatherIcon.setImageResource(getResources().getIdentifier(icon, "drawable", getPackageName()));
                    weatherDescTxt.setText(weatherDesc.getString("description"));
                    cloudTxt.setText(Double.toString(weatherCloud.getDouble("all")));
                    humidityTxt.setText(Integer.toString(weatherTemp.getInt("humidity")));
                    curTempTxt.setText(getCelsius(weatherTemp.getDouble("temp")));
                    minTempTxt.setText(getCelsius(weatherTemp.getDouble("temp_min")));
                    maxTempTxt.setText(getCelsius(weatherTemp.getDouble("temp_max")));

                    Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();

                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), "Error retrieving data", Toast.LENGTH_SHORT).show();
            }
        };

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, apiUrl, null, responseListener, errorListener);
        RequestQueueSingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }
}
